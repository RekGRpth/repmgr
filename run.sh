#!/bin/sh -ex

#docker build --tag rekgrpth/repmgr .
#docker push rekgrpth/repmgr
docker pull rekgrpth/repmgr
docker network create --attachable --opt com.docker.network.bridge.name=docker docker || echo $?
docker volume create repmgr
docker stop repmgr || echo $?
docker rm repmgr || echo $?
docker run \
    --detach \
    --env GROUP_ID="$(id -g)" \
    --env LANG=ru_RU.UTF-8 \
    --env TZ=Asia/Yekaterinburg \
    --env USER_ID="$(id -u)" \
    --hostname repmgr \
    --mount type=bind,source=/etc/certs,destination=/etc/certs,readonly \
    --mount type=volume,source=repmgr,destination=/var/lib/postgresql \
    --name repmgr \
    --network name=docker \
    --publish target=5432,published=5432,mode=host \
    --restart always \
    rekgrpth/repmgr /etc/service/postgres/single
