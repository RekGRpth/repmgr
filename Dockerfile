FROM alpine
RUN exec 2>&1 \
    && set -ex \
    && apk add --no-cache --virtual .postgresql-rundeps \
        musl-locales \
        openssh-client \
        openssh-server \
        pgbouncer \
        postgresql \
        postgresql-contrib \
        repmgr \
        repmgr-daemon \
        rsync \
        runit \
        shadow \
        tzdata \
    && rm -rf /usr/src /usr/share/doc /usr/share/man /usr/local/share/doc /usr/local/share/man \
    && echo done
ADD bin /usr/local/bin
ADD multi /etc/multi
CMD [ "runsvdir", "/etc/multi" ]
ENTRYPOINT [ "docker_entrypoint.sh" ]
ENV HOME=/var/lib/postgresql
ENV GROUP=postgres \
    PGDATA="${HOME}/pg_data" \
    USER=postgres
VOLUME "${HOME}"
WORKDIR "${HOME}"
RUN exec 2>&1 \
    && set -ex \
    && sed -i -e 's|#PasswordAuthentication yes|PasswordAuthentication no|g' /etc/ssh/sshd_config \
    && sed -i -e 's|#   StrictHostKeyChecking ask|   StrictHostKeyChecking no|g' /etc/ssh/ssh_config \
    && echo "   UserKnownHostsFile=/dev/null" >>/etc/ssh/ssh_config \
    && sed -i -e 's|postgres:!:|postgres::|g' /etc/shadow \
    && chmod -R 0755 /etc/multi /usr/local/bin \
    && rm -rf /usr/src /usr/share/doc /usr/share/man /usr/local/share/doc /usr/local/share/man \
    && echo done
