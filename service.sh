#!/bin/sh -ex

#docker build --tag rekgrpth/repmgr .
#docker push rekgrpth/repmgr
docker pull rekgrpth/repmgr
docker network create --attachable --driver overlay docker || echo $?
docker volume create repmgr
docker service rm repmgr || echo $?
docker service create \
    --env GROUP_ID="$(id -g)" \
    --env LANG=ru_RU.UTF-8 \
    --env TZ=Asia/Yekaterinburg \
    --env USER_ID="$(id -u)" \
    --hostname repmgr-{{.Node.Hostname}} \
    --mode global \
    --mount type=bind,source=/etc/certs,destination=/etc/certs,readonly \
    --mount type=bind,source=/mnt/gfs/repmgr,destination=/var/lib/postgresql/gfs \
    --mount type=volume,source=repmgr,destination=/var/lib/postgresql \
    --name repmgr \
    --network name=docker \
    --publish target=5432,published=5432,mode=host \
    --publish target=5433,published=5433,mode=host \
    rekgrpth/repmgr
